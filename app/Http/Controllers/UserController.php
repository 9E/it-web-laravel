<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Validation\ValidationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use PHPUnit\Exception;

class UserController extends Controller
{
    public function getAllNotes($id)
    {
        $user = User::find($id);

        if (!$user) {
            return response(['message' => 'User not found'], 404);
        }

        return $user->notes;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::all();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return User::find($id);
    }

    public function register(Request $request)
    {
        $validated = $request->validate([
            'username' => 'required|unique:users|max:50',
            'password' => 'required|confirmed|max:100'
        ]);

        $user = User::create([
            'username' => $validated['username'],
            'password' => Hash::make($validated['password'])
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        return response([
            'user' => $user,
            'authToken' => [
                'token' => $token,
                'expiresAfter' => config('sanctum.expiration')
            ]
        ], 201);
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'username' => 'required|max:50',
            'password' => 'required|max:100'
        ]);

        $user = User::where('username', $validated['username'])->first();

        if (!$user || !Hash::check($validated['password'], $user->password)) {
            throw ValidationException::withMessages([
                'username' => ['Wrong credentials']
            ]);
        }

        return [
            'token' => $user->createToken($request->username)->plainTextToken,
            'expiresAfter' => config('sanctum.expiration')
        ];
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->currentAccessToken()->delete();

        return [
            'message' => 'User ' . $user['username'] . ' has successfully logged out'
        ];
    }

    public function identifyUserByToken()
    {
        return auth()->user();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $validated = $request->validate([
            'username' => 'required|unique:users|max:50',
            'password' => 'required|max:100'
        ]);

        $user = User::find($id);

        if (!$user) {
            return response(['message' => 'User not found'], 404);
        }

        $authUser = auth()->user();
        if ($user['id'] !== $authUser['id']) {
            return response(['message' => 'User is not authorized to do this action'], 403);
        }

        $user->update($validated);
        return $user;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $user = User::find($id);

        if (!$user) {
            return response(['message' => 'User not found'], 404);
        }

        $authUser = auth()->user();
        if ($user['id'] !== $authUser['id']) {
            return response(['message' => 'User is not authorized to do this action'], 403);
        }

        return (User::destroy($id) === 1) ?
            $user :
            response(['message' => 'Failed to delete user'], 500);
    }
}
