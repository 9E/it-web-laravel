<?php

namespace App\Http\Controllers;

use App\Models\Note;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoteController extends Controller
{
    public function getUser($id)
    {
        $note = Note::find($id);

        if (!$note) {
            return response(['message' => 'Note not found'], 404);
        }

        return $note->user;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Note::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
//            'user_id' => 'required|exists:users,id',
            'name' => 'max:255',
            'text' => 'max:1500'
        ]);

        return Note::create([
//            'user_id' => $request->input('user_id'),
            'user_id' => auth()->user()['id'],
            'name' => $request->input('name'),
            'text' => $request->input('text')
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        return Note::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, int $id)
    {
        $request->validate([
//            'user_id' => 'required|exists:users,id',
            'name' => 'max:255',
            'text' => 'max:1500'
        ]);

        $note = Note::find($id);

        if (!$note) {
            return response(['message' => 'Note not found'], 404);
        }

        $user = auth()->user();
        if ($note['user_id'] !== $user['id']) {
            return response(['message' => 'User is not authorized to do this action'], 403);
        }

        $note->update([
            'user_id' => $user['id'],
            'name' => $request->name,
            'text' => $request->text
        ]);
        return $note;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $note = Note::find($id);

        if (!$note) {
            return response(['message' => 'Note not found'], 404);
        }

        $user = auth()->user();
        if ($note['user_id'] !== $user['id']) {
            return response(['message' => 'User is not authorized to do this action'], 403);
        }

        return (Note::destroy($id) === 1) ?
            $note :
            response(['message' => 'Failed to delete note'], 500);
    }
}
